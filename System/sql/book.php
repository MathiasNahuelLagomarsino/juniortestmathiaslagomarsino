<?php
class book extends product{

    public $Weight;

    public function __Construct($Sku,$Name,$Price,$Weight)
    {
        parent:: __Construct($Sku,$Name,$Price);
        $this->Weight=$Weight;
    }
    public function showData()
    {

        $Sql = "SELECT * from book;";
        $result = mysqli_query($this->Connection, $Sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}