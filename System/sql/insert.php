<?php
    require_once "connection.php";
    require_once "QueryBuild.php";
    require_once "dvd.php";
    require_once "furniture.php";
    require_once "book.php";
//Here I send data from add.php to mysql
    require_once "product.php";
    $config = require 'config.php';
    $Sku = $_POST['sku'];
    $Name = $_POST['name'];
    $Price = $_POST['price'];
    $Type = $_POST['type'];


    $queryBuilder = new QueryBuilder(
        Connection::make($config['database'])
    );

    switch ($Type) {
        case 1:
            $Height = $_POST['height'];
            $Width = $_POST['width'];
            $Length = $_POST['length'];
            
            $furniture = new furniture($Sku,$Name,$Price,$Height,$Width,$Length);

            $queryBuilder->insert('furniture',[
                'sku' => $furniture->Sku,
                'Name' => $furniture->Name,
                'Price' => $furniture->Price,
                'Height' => $furniture-> Height,
                'Width' => $furniture->Width,
                'Length' => $furniture->Length
            ]);
            
            break;
        case 2:

            $Weight = $_POST['weight'];
            
            $book = new book($Sku,$Name,$Price,$Weight);

            $queryBuilder->insert('book',[
                'sku' => $book->Sku,
                'Name' => $book->Name,
                'Price' => $book->Price,
                'Weight' => $book->Weight
                ]);

            break;
        case 3:
            $Size = $_POST['size'];
            $dvd = new dvd($Sku,$Name,$Price,$Size);
            $queryBuilder->insert('dvd',[
                'sku' => $dvd->Sku,
                'Name' => $dvd->Name,
                'Price' => $dvd->Price,
                'Size' => $dvd->Size
                ]);
            break;
    }


    header('Location:../../index.php');



