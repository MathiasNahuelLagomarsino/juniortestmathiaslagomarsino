<?php
class furniture extends product{

    public $Height;
    public $Width;
    public $Length;

    public function __Construct($Sku,$Name,$Price,$Height,$Width,$Length)
    {
        parent:: __Construct($Sku,$Name,$Price);
        $this->Height=$Height;
        $this->Width=$Width;
        $this->Length=$Length;
    }
    public function showData()
    {

        $Sql = "SELECT * from furniture;";
        $result = mysqli_query($this->Connection, $Sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}
