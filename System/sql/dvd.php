<?php
include_once("product.php");
class dvd extends product{

    public $Size;

    public function __Construct($Sku,$Name,$Price,$Size)
    {
        parent::__Construct($Sku,$Name,$Price);
        $this->Size=$Size;
        
    }

    public function showData()
    {

        $Sql = "SELECT * from dvd;";
        $result = mysqli_query($this->Connection, $Sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}
