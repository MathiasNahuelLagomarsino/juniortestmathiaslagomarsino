<?php
class QueryBuilder
{

    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function select(string $table)
    {
        $query = $this->pdo->prepare("SELECT * FROM $table");
        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function insert(string $table, array $data)
    {
        $keys = implode(', ', array_keys($data));
        $values = ':' . implode(', :', array_keys($data));
        $tableName="\"$table\"";
        try {
            $queryProduct = $this->pdo->prepare("INSERT INTO product(type)values($tableName);");
            $queryProduct->execute($data);
            $queryId=$this->pdo->query("SELECT id from product order by id desc limit 1;")->fetch();
            $query = $this->pdo->prepare("INSERT INTO $table (id,$keys) VALUES ($queryId[0],$values)");
            return $query->execute($data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}