<?php

namespace Scandiweb;

use \PDO;

return [
    'database' => [
        'driver' => 'mysql',
        'database' => 'id16168167_scandiweb',
        'user' => 'root',
        'password' => '',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ],
    ],
];
