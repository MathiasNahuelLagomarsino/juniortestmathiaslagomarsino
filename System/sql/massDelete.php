<?php
require_once "product.php";

//Here I collect all of the checkboxes that are selected from index.php and delete them in mysql

if (!empty($_POST['id'])) {
    $ConnectionFile = "connection.ini";
    $Configuration = parse_ini_file($ConnectionFile, true);
    $Host = $Configuration["DataBase"]["Host"];
    $DataBase = $Configuration["DataBase"]["DataBase"];
    $User = $Configuration["DataBase"]["User"];
    $Password = $Configuration["DataBase"]["Password"];
    $Connection = new mysqli($Host,$User,$Password, $DataBase);

        foreach ($_POST['id'] as $IdNumber) {
            $Sql = "call SP_Product_Delete($IdNumber);";
            $Connection->query($Sql);
        }
    
    header('Location:../../index.php');
}
