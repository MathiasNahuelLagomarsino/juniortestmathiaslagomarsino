<?php

//This is the main object, here I set the connections and call the Stored Procedures
abstract class product
{
    public $Host;
    public $DataBase;
    public $User;
    public $Password;
    public $Connection;

    public $Sku;
    public $Name;
    public $Price;
    
    public function __Construct($Sku,$Name,$Price)
    {
        //I create the connection here.
        $ConnectionFile = "connection.ini";
        $Configuration = parse_ini_file($ConnectionFile, true);
        $this->Host = $Configuration["DataBase"]["Host"];
        $this->DataBase = $Configuration["DataBase"]["DataBase"];
        $this->User = $Configuration["DataBase"]["User"];
        $this->Password = $Configuration["DataBase"]["Password"];
        $this->Connection = new mysqli($this->Host, $this->User,$this->Password, $this->DataBase);

        $this->Sku=$Sku;
        $this->Name=$Name;
        $this->Price=$Price;


    }
    


    
}
























  /*  //This method calls the Stored Procedure and sends data to it
    protected function insertProduct($data)
    {

        $Sql= "CALL SP_Product_Add('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]','$data[8]')";

        return mysqli_query($this->Connection, $Sql);


    }

    //This method is used in index.php and calls a Stored Procedure that deletes the desired product
    public function massDelete($data)
    {
        $Sql = "CALL SP_Product_Delete($data)";
        mysqli_query($this->Connection, $Sql);
    }*/

