<?php
include_once("./System/header.php");
$conn = include_once("./System/sql/product.php");

include_once "System/sql/connection.php";
include_once "System/sql/QueryBuild.php";
include_once "System/sql/dvd.php";
include_once "System/sql/furniture.php";
include_once "System/sql/book.php";

?>
<title>Product list</title>
<link rel="stylesheet" type="text/css" href=".\System\Bootstrap\sass\custom.css">
</head>
<body>
<!--Bar-->
<form action="./System/sql/massDelete.php" method="post">
    <div class="row m-0">
        <div class="col-lg-9 col-md-7 col-12 d-flex align-items-end mb-4 mt-4">
            <h1>Product List</h1>
        </div>
        <div class="col-lg-1 col-md-2 col-4">
            <a href="Product/add.php" class="btn primary mt-4">ADD</a>
        </div>
        <div class="col-lg-2 col-md-3 col-8">
            <button type="submit" value="massDelete" class="btn danger mt-4"><a>MASS
                    DELETE</a></button>
        </div>
    </div>
    <hr class="ml-3 mr-3 m-0">
    <!--List-->
    <main>
    <div class="row m-5">
            <!--Products-->
            <?php
            $dvd = new dvd(0,0,0,0);
            $Data = $dvd->showData();
            foreach ($Data as $Key) {
                $Id = $Key['id'];
                $Sku = $Key['sku'];
                $Name = $Key['name'];
                $Price = $Key['price'];
                $Size = $Key['size'];

                ?>
                <div class="col-lg-3 col-sm- col-md-6 mb-4">
                    <div class="card border-5 border-dark"
                         style="width: 12rem;">
                        <div class="row">
                            <div class="col-1">
                                <div class="form-check">
                                    <input class="form-check-input"
                                           type="checkbox"
                                           value="<?php echo $Id ?>" name="id[]"
                                           class="productID"">
                                </div>
                            </div>
                            <div class="col-10">
                                <br>
                                <p class="card-text text-center m-0 mt-4"><?php echo $Sku ?></p>
                                <p class="card-text text-center m-0"><?php echo $Name ?></p>
                                <p class="card-text text-center m-0"><?php echo $Price ?></p>
                                <p class="card-text text-center m-0 mb-5"><?php
                                    echo "Size: " . $Size . "<br><br>";
                                    ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        </div>

        <div class="row m-5">
            <!--Products-->
            <?php
            $furniture = new furniture(0,0,0,0,0,0);
            $DataF = $furniture->showData();
            
            foreach ($DataF as $Key) {
                $Id = $Key['ID'];
                $Sku = $Key['sku'];
                $Name = $Key['name'];
                $Price = $Key['price'];
                $Height = $Key['height'];
                $Width = $Key['width'];
                $Length = $Key['length'];

                ?>
                <div class="col-lg-3 col-sm- col-md-6 mb-4">
                    <div class="card border-5 border-dark"
                         style="width: 12rem;">
                        <div class="row">
                            <div class="col-1">
                                <div class="form-check">
                                    <input class="form-check-input"
                                           type="checkbox"
                                           value="<?php echo $Id ?>" name="id[]"
                                           class="productID">
                                </div>
                            </div>
                            <div class="col-10">
                                <br>
                                <p class="card-text text-center m-0 mt-4"><?php echo $Sku ?></p>
                                <p class="card-text text-center m-0"><?php echo $Name ?></p>
                                <p class="card-text text-center m-0"><?php echo $Price ?></p>
                                <p class="card-text text-center m-0 mb-5"><?php

                                            echo "Dimension: " . $Height . "X"
                                                . $Width . "X" . $Length
                                                . "<br>";
                                    ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        </div>
        <div class="row m-5">
            <!--Products-->
            <?php
            $book = new book(0,0,0,0);
            $DataB = $book->showData();
            foreach ($DataB as $Key) {
                $Id = $Key['ID'];
                $Sku = $Key['sku'];
                $Name = $Key['name'];
                $Price = $Key['price'];
                $Weight = $Key['weight'];
                ?>
                <div class="col-lg-3 col-sm- col-md-6 mb-4">
                    <div class="card border-5 border-dark"
                         style="width: 12rem;">
                        <div class="row">
                            <div class="col-1">
                                <div class="form-check">
                                    <input class="form-check-input"
                                           type="checkbox"
                                           value="<?php echo $Id ?>" name="id[]"
                                           class="productID"">
                                </div>
                            </div>
                            <div class="col-10">
                                <br>
                                <p class="card-text text-center m-0 mt-4"><?php echo $Sku ?></p>
                                <p class="card-text text-center m-0"><?php echo $Name ?></p>
                                <p class="card-text text-center m-0"><?php echo $Price ?></p>
                                <p class="card-text text-center m-0 mb-5"><?php
                                    echo "Weight: " . $Weight
                                    . "<br><br>";
                                    ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        </div>            
                                    
        <!--Footer-->
    </main>
    <hr class="ml-3 mr-3 m-0">
    <h5 class="text-center mt-4">Scandiweb Test assignment</h5>
</form>
</body>
</html>