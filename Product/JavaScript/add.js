$(document).ready(() => {
});


//Making a generic function to delete last type
function del() {
    $(".newInput").fadeOut(300, function () {
        $(this).remove();
    });
}


//Functions to append the data types to the page
function appendFurniture() {

    del();
    var inputHeight = ("<div class='form-group row newInput'> " +
        "<label id='size' class='col-sm-1 col-form-label'>Height (CM)</label> " +
        "<div class='col-sm-3'>" +
        "<input type='text' class='form-control' name='height' placeholder='Please, provide Height'>" +
        "</div></div>");
    var inputWidth = ("<div class='form-group row newInput'> " +
        "<label id='size' class='col-sm-1 col-form-label'>Width (CM)</label> " +
        "<div class='col-sm-3'>" +
        "<input type='text' class='form-control'  name='width' placeholder='Please, provide Width'>" +
        "</div></div>");
    var inputLength = ("<div class='form-group row newInput'> " +
        "<label id='size' class='col-sm-1 col-form-label'>Length (CM)</label> " +
        "<div class='col-sm-3'>" +
        "<input type='text' class='form-control'  name='length' placeholder='Please, provide Length'>" +
        "</div></div>");
    setTimeout(function () {
        $(".divType").append(inputHeight + inputWidth + inputLength).fadeIn(300);
    }, 300);
}


function appendDvd() {
    del();
    var inputSize = ("<div class='form-group row newInput'> " +
        "<label id='size' class='col-sm-1 col-form-label'>Size (MB)</label> " +
        "<div class='col-sm-3'>" +
        "<input type='text' class='form-control'  name='size' placeholder='Please, provide size'>" +
        "</div></div>");
    setTimeout(function () {
        $(".divType").append(inputSize);
    }, 300);

}


function appendBook(book) {
    del();
    var inputWeight = ("<div class='form-group row newInput'> " +
        "<label id='size' class='col-sm-1 col-form-label'>Weight (KG)</label> " +
        "<div class='col-sm-3'>" +
        "<input type='text' class='form-control'  name='weight' placeholder='Please, provide Weight'>" +
        "</div></div>");
    setTimeout(function () {
        $(".divType").append(inputWeight).fadeIn(300);
    }, 300);
}
