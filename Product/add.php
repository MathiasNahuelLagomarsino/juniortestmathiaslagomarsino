<?php
include_once("../System/header.php");

include_once "../System/sql/connection.php";
include_once "../System/sql/QueryBuild.php";
include_once "../System/sql/dvd.php";
?>
<script type="text/javascript" src="../System/js/jquery-3.5.1.js"></script>
<script type="text/javascript" src="JavaScript/add.js"></script>
<title>Add Product</title>
</head>

<!--Bar-->
<body>

<form action="../System/sql/insert.php" method="post">
    <div class="row m-0 topDiv">
        <div class="col-lg-9 col-md-7 col-12 d-flex align-items-end mb-4 mt-4">
            <h1>Product Add</h1>
        </div>

        <div class="col-lg-1 col-md-2 col-4">
            <button type="submit" value="Submit" class="btn primary mt-4"
                    href="../index.php"><a>Save</a></button>
        </div>
        <div class="col-lg-2 col-md-3 col-8 ">
            <button type="button" class="btn btn-secondary mt-4"
                    href="../index.php"><a>Cancel</a></button>
        </div>
    </div>
    <hr class="m-0">
    <main>
        <div class="m-5 divType">


            <div class="form-group row mb-2">
                <label for="sku" class="col-sm-1 col-form-label">SKU</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="sku"
                           placeholder="Please, provide SKU" required>
                </div>
            </div>
            <div class="form-group row mb-2">
                <label for="inputPassword3"
                       class="col-sm-1 col-form-label">Name</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="name"
                           placeholder="Please, provide name">
                </div>
            </div>
            <div class="form-group row mb-2">
                <label for="price" class="col-sm-1 col-form-label">Price
                    ($)</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="price"
                           placeholder="Please, provide price">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 col-form-label"
                       for="type">Product:</label>
                <div class="col-sm-3 col-form-label">
                    <select class="form-control" name="type">
                        <option disabled>Please, choose type</option>
                        <option value="3" onclick="appendDvd()">DVD</option>
                        <option value="2" onclick="appendBook()">Book</option>
                        <option value="1" onclick="appendFurniture()">Forniture</option>
                    </select>
                </div>
            </div>
        </div>
        </div>
        <!--Footer-->
    </main>
    <hr class="m-0">
    <h5 class="text-center mt-4">Scandiweb Test assignment</h5>
</form>
</body>
</html>